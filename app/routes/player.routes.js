module.exports = app => {
  const players = require("../controllers/player.controller.js");

  var router = require("express").Router();

  // Player Endpoints
  router.post("/players", players.create);
  router.get("/players/list", players.findAll);
  router.get("/players/:id", players.findById);
  router.put("/players/update/:id", players.update);
  router.post("/players/exp/:id", players.getExperience);
  router.delete("/players/delete/:id", players.delete);

  // API prefix
  app.use("/api", router);
};
