import { Container } from "react-bootstrap";

function App() {
  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
      <Container>
        <h1 className="text-center">Dandi Christiawan</h1>
        <h2 className="text-center">Binar Academy</h2>
        <h3 className="text-center">FSW Development Chapter 8</h3>
      </Container>
    </div>
  );
}

export default App;
