import { useState } from 'react';
import { Container } from 'react-bootstrap';
import EditForm from "../components/editForm";
import NewForm from "../components/newForm";
import SearchForm from '../components/searchForm';

const Player = () => {
  const [players, setPlayers] = useState([]);
  const [selectedPlayerIndex, setSelectedPlayerIndex] = useState(null);
  const [displayedPlayers, setDisplayedPlayers] = useState([]);

  const handleEdit = (editedPlayer) => {
    const updatedPlayers = [...players];
    updatedPlayers[selectedPlayerIndex] = editedPlayer;
    setPlayers(updatedPlayers);
    setSelectedPlayerIndex(null);
    setDisplayedPlayers(updatedPlayers);
  };

  const handleAdd = (newPlayer) => {
    setPlayers([...players, newPlayer]);
    setDisplayedPlayers([...players, newPlayer]);
  };

  const handleSearch = (query) => {
    const filteredPlayers = players.filter(player => {
      const { username, email, experience, level } = player;
      return (
        username.includes(query) ||
        email.includes(query) ||
        experience.toString().includes(query) ||
        level.toString().includes(query)
      );
    });
    setDisplayedPlayers(filteredPlayers);
  };

  const handleClear = () => {
    setDisplayedPlayers(players);
  };

  return (
    <Container className="mt-5">
      {selectedPlayerIndex !== null && (
        <>
          <h4>Edit Player:</h4>
          <p>Username: {players[selectedPlayerIndex].username}</p>
          <p>Email: {players[selectedPlayerIndex].email}</p>
          <p>Experience: {players[selectedPlayerIndex].experience}</p>
          <p>Level: {players[selectedPlayerIndex].level}</p>
          <hr />
          <EditForm player={players[selectedPlayerIndex]} onEdit={handleEdit} />
          <hr />
        </>
      )}
      {(displayedPlayers.length > 0 ? displayedPlayers : players).map((player, index) => (
        <div key={index}>
          <h4 className='mb-3'>Player {index + 1}:</h4>
          <p>Username: {player.username}</p>
          <p>Email: {player.email}</p>
          <p>Experience: {player.experience}</p>
          <p>Level: {player.level}</p>
          <button className="btn btn-primary" onClick={() => setSelectedPlayerIndex(index)}>Edit</button>
        </div>
      ))}
      <hr />
      <NewForm onAdd={handleAdd} />
      <hr />
      <SearchForm onSearch={handleSearch} onClear={handleClear} />
    </Container>
  );
}

export default Player;
