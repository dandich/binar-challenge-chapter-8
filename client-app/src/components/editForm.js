import { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Container } from 'react-bootstrap';

const EditForm = ({ player, onEdit }) => {

  const [values, setValues] = useState(player);


  useEffect(() => {
    setValues({
      username: player.username || '',
      email: player.email || '',
      experience: player.experience || '',
      level: player.level || ''
    });
  }, [player]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onEdit(values)
  };

  return (
    <>
      <Container>
        <Form onSubmit={handleSubmit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" value={values.username} onChange={handleInputChange} name="username" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" value={values.email} onChange={handleInputChange} name="email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicExperience">
            <Form.Label>Experience</Form.Label>
            <Form.Control type="number" value={values.experience} onChange={handleInputChange} name="experience" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicLevel">
            <Form.Label>Level</Form.Label>
            <Form.Control type="number" value={values.level} onChange={handleInputChange} name="level" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Save
          </Button>
        </Form>
      </Container>
    </>
  );
}

export default EditForm;

