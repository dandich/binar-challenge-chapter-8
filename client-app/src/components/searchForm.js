import { useState } from "react";
import Form from 'react-bootstrap/Form';
import { Container, Row, Col, Button } from 'react-bootstrap';

const SearchForm = ({ onSearch, onClear }) => {

  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = (e) => {
    e.preventDefault();
    onSearch(searchTerm);
  }

  const handleClear = () => {
    setSearchTerm('');
    onClear();
  }

  return (
    <Container>
      <h4 className="mb-3">Search Player</h4>
      <Form onSubmit={handleSearch}>
        <Row className="mb-3">
          <Col xs={9}>
            <Form.Control type="text" value={searchTerm} onChange={(event) => setSearchTerm(event.target.value)} placeholder="Search Player" />
          </Col>
          <Col xs={3}>
            <Button type="submit" variant="primary">Search</Button>
            <Button variant="outline-secondary" onClick={handleClear}>Clear</Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default SearchForm;
