import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Player from './pages/playerPage';
import { appNavbar as Navbar } from './components/Navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <Navbar />
      <Routes>
        <Route exact path='/' element={<App />} />
        <Route exact path='/player' element={<Player />} />
      </Routes>
    </Router>
  </React.StrictMode>
);

